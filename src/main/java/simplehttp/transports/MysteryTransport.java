package simplehttp.transports;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@AllArgsConstructor
public class MysteryTransport {
    private final HttpTransport httpTransport;
    private final String url = "http://localhost:8080/api/query";

    public Map<String, Object> enquire(Map<String, Object> payload) throws Exception {
        return httpTransport.post(url, payload);
    }
}
