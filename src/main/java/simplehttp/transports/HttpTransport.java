package simplehttp.transports;

public interface HttpTransport {
    <T> T post(String path, Object payload) throws Exception;
}