package simplehttp.transports;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import simplehttp.exceptions.HttpTransportException;

import java.io.IOException;

@Component
@AllArgsConstructor
public class RealHttpTransport implements HttpTransport {

    private final ObjectMapper marshaller = new ObjectMapper();
    private final HttpClient client = HttpClientBuilder.create().build();

    private static HttpUriRequest setHeaders(HttpUriRequest request) {
        request.setHeader(HttpHeaders.ACCEPT, "application/json");
        request.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        return request;
    }

    @Override
    public <T> T post(String path, Object payload) throws Exception {
        HttpEntity entity = new StringEntity(marshaller.writeValueAsString(payload));
        HttpUriRequest request = RequestBuilder.post(path)
                .setEntity(entity).build();
        HttpResponse response = client.execute(setHeaders(request));
        return unmarshallJson(response.getStatusLine().getStatusCode(),
                EntityUtils.toString(response.getEntity()));
    }

    private <T> T unmarshallJson(int statusCode, String rawJson) throws IOException {
        if (!(String.valueOf(statusCode)).startsWith("2")) {
            throw new HttpTransportException(statusCode);
        }
        return marshaller.readValue(rawJson, new TypeReference<T>() {
        });
    }
}
