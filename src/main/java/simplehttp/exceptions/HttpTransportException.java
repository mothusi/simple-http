package simplehttp.exceptions;

public class HttpTransportException extends RuntimeException {
    public HttpTransportException(int statusCode) {
        super("Http request failed: status code: " + statusCode);
    }
}
