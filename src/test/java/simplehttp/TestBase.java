package simplehttp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import simplehttp.Fakes.Fake;

import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public abstract class TestBase {
    protected final ObjectMapper mapper = new ObjectMapper();
    @Autowired
    public List<Fake> fakes;

    @Before
    public void tearDown() {
        fakes.forEach(Fake::reset);
    }

}