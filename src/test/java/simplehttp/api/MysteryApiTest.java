package simplehttp.api;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import simplehttp.Fakes.FakeHttpTransport;
import simplehttp.TestBase;
import simplehttp.exceptions.HttpTransportException;
import simplehttp.testers.MysteryTester;
import simplehttp.transports.MysteryTransport;

import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class MysteryApiTest extends TestBase {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    @Autowired
    public MysteryTransport mysteryTransport;
    @Autowired
    public FakeHttpTransport fakeHttpTransport;

    @Before
    public void setUp() {
        fakeHttpTransport.respondsWith(MysteryTester.getSampleResponse());
    }

    @Test
    public void canSuccessfullySubmitMysteryRequest() throws Exception {
        Map<String, Object> response = mysteryTransport.enquire(MysteryTester.getSampleRequest());
        assertThat(response, is(MysteryTester.getSampleResponse()));
        assertEquals(1, fakeHttpTransport.getRequests().size());
        Map<String, Object> request = fakeHttpTransport.getLastRequest();
        assertEquals("http://localhost:8080/api/query", request.get("path"));
        assertEquals(mapper.writeValueAsString(MysteryTester.getSampleRequest()), request.get("payload"));
    }

    @Test
    public void throwsErrorForUnknownHost() throws Exception {
        fakeHttpTransport.bringDown();
        exception.expect(HttpTransportException.class);
        exception.expectMessage("Http request failed: status code: 404");
        mysteryTransport.enquire(MysteryTester.getSampleRequest());
    }

}
