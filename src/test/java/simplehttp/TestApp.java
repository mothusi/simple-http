package simplehttp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import simplehttp.Fakes.FakeHttpTransport;
import simplehttp.transports.HttpTransport;

@Import(Application.class)
public class TestApp {

    private final FakeHttpTransport fakeHttpTransport = new FakeHttpTransport();

    @Bean
    HttpTransport httpTransport() {
        return fakeHttpTransport;
    }

}