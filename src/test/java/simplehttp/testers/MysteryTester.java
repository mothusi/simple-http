package simplehttp.testers;

import java.util.HashMap;
import java.util.Map;

public class MysteryTester {

    public static Map<String, Object> getSampleRequest() {
        Map<String, Object> request = new HashMap<>();
        request.put("uuid", "12345");
        request.put("type", "mystery");
        return request;
    }

    public static Map<String, Object> getSampleResponse() {
        Map<String, Object> response = new HashMap<>();
        response.put("status", "Success");
        response.put("uuid", "12345");
        return response;
    }
}
