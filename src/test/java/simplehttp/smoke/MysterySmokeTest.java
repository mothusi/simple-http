package simplehttp.smoke;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import simplehttp.TestBase;
import simplehttp.exceptions.HttpTransportException;
import simplehttp.testers.MysteryTester;
import simplehttp.transports.MysteryTransport;
import simplehttp.transports.RealHttpTransport;

public class MysterySmokeTest extends TestBase {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private final MysteryTransport transport = new MysteryTransport(new RealHttpTransport());

    @Test
    public void throwsErrorForUnknownHost() throws Exception {
        exception.expect(HttpTransportException.class);
        exception.expectMessage("Http request failed: status code: 404");
        transport.enquire(MysteryTester.getSampleRequest());
    }
}
