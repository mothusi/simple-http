package simplehttp.Fakes;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.CollectionUtils;
import simplehttp.exceptions.HttpTransportException;
import simplehttp.transports.HttpTransport;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FakeHttpTransport implements HttpTransport, Fake {
    private List<Map<String, Object>> requests = new LinkedList<>();
    private LinkedList<Map<String, Object>> responses = new LinkedList<>();
    private boolean isUp = true;
    private ObjectMapper marshaller = new ObjectMapper();

    @Override
    public <T> T post(String path, Object payload) throws Exception {
        Map<String, Object> request = new HashMap<>();
        request.put("path", path);
        request.put("payload", marshaller.writeValueAsString(payload));
        requests.add(request);
        if (!isUp) {
            throw new HttpTransportException(404);
        }
        return (T) responses.poll();
    }

    public Map<String, Object> getLastRequest() {
        if (CollectionUtils.isEmpty(requests)) {
            return null;
        }
        return requests.get(requests.size() - 1);
    }

    public List<Map<String, Object>> getRequests() {
        return requests;
    }

    public void respondsWith(Map<String, Object> toRespond) {
        responses.offer(toRespond);
    }

    @Override
    public void reset() {
        responses.clear();
        requests.clear();
        isUp = true;
    }

    public void bringDown() {
        isUp = false;
    }
}
